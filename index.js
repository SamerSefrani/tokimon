const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

const { Pool } = require('pg');
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  //ssl: true
});
pool.connect(function(){
  console.log("Connected to db");
})

express()
  .use(express.static(path.join(__dirname, 'public')))
  .use(express.json())
  .use(express.urlencoded({extended:false}))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => {
    createTablesForTokimons()
    res.render('pages/index')})
  .post('/', async (req, res) => {
    console.log('post');
    var tokimon_name = req.body.tokimon_name;
    var trainer_name = req.body.trainer_name;
    var height = parseInt(req.body.height);
    var fly = parseInt(req.body.fly);
    var fight = parseInt(req.body.fight);
    var fire = parseInt(req.body.fire);
    var water = parseInt(req.body.water);
    var electric = parseInt(req.body.electric);
    var frozen = parseInt(req.body.frozen);
    var weight = parseInt(req.body.weight);
    var total = parseInt(height+fly+fight+fire+water+electric+frozen+weight);
    console.log("hohoohoh"+total)
    pool.query(`INSERT INTO tokimons (name,name_of_trainer,height,fly,fight,fire,water,electric,frozen,total,weight,created_date) VALUES ('${tokimon_name}', '${trainer_name}',${height}, ${fly}, ${fight},${fire}, ${water}, ${electric},${frozen},${total},${weight},null);`);
    res.render('pages/index')
    
  })
  .get('/', async (req, res) => {
    // try {
    //   const client = await pool.connect()
    //   const result = await client.query('SELECT * FROM tokimons WHERE total::integer>100');
    //   var resultsForMaxTotal = { 'resultsForMaxTotal': (result) ? result.rows : null};
    //   res.render('pages/index', resultsForMaxTotal);
    //   console.log("f0f0f0f"+resultsForMaxTotal)
    //   client.release();
    // } catch (err) {
    //   console.error(err);
    //   res.send("Error " + err);
    // }
  })
  .get('/db', async (req, res) => {
    try {
      const client = await pool.connect()
      const result = await client.query('SELECT * FROM tokimons');
      const results = { 'results': (result) ? result.rows : null};

      res.render('pages/db', results);
      client.release();
    } catch (err) {
      console.error(err);
      res.send("Error " + err);
    }
    

  })
  .get('/tokimonDetails/:id', async (req,res) => {
    try {
      const client = await pool.connect()
      const result = await client.query(`SELECT * FROM tokimons WHERE uid=${req.params.id}`);
      const results = { 'results': (result) ? result.rows : null};
      console.log(results);
      res.render('pages/details', results );
      client.release();
    } catch (err) {
      console.error(err);
      res.send("Error " + err);
    }
  })
  .post('/delete',async(req,res)=>{
    var id = req.body.uid;
    pool.query(`delete from tokimons where uid=${id}`,(error,result)=>{
      if(error)
        res.send(error)
    });
    try {
      const client = await pool.connect()
      const result = await client.query('SELECT * FROM tokimons');
      const results = { 'results': (result) ? result.rows : null};
      res.render('pages/db', results );
      client.release();
    } catch (err) {
      console.error(err);
      res.send("Error " + err);
    }
  })
  .post('/edit',async(req,res)=>{
    var id = req.body.uid;
    var tokimon_name = req.body.tokimon_name;
    var trainer_name = req.body.trainer_name;
    var height = parseInt(req.body.height);
    var fly = parseInt(req.body.fly);
    var fight = parseInt(req.body.fight);
    var fire = parseInt(req.body.fire);
    var water = parseInt(req.body.water);
    var electric = parseInt(req.body.electric);
    var frozen = parseInt(req.body.frozen);
    var weight = parseInt(req.body.weight);
    var total = parseInt(height+fly+fight+fire+water+electric+frozen+weight);
    pool.query(`update tokimons set name = '${tokimon_name}',name_of_trainer = '${trainer_name}',height = ${height},fly = ${fly},fight = ${fight},fire = ${fire},water = ${water},electric = ${electric},frozen = ${frozen},total = ${total},weight = ${weight} where uid=${id}`,(error,result)=>{
      if(error)
        res.send(error)
    });
    try {
      const client = await pool.connect()
      const result = await client.query('SELECT * FROM tokimons');
      const results = { 'results': (result) ? result.rows : null};
      res.render('pages/db', results );
      client.release();
    } catch (err) {
      console.error(err);
      res.send("Error " + err);
    }
  })
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))


  const createTablesForTokimons = () => {
    const queryText =
      `CREATE TABLE IF NOT EXISTS
        tokimons(
          uid SERIAL,
          name TEXT NOT NULL,
          name_of_trainer TEXT NOT NULL,
          height VARCHAR(128) NOT NULL,
          fly VARCHAR(128) NOT NULL,
          fight VARCHAR(128) NOT NULL,
          fire VARCHAR(128) NOT NULL,
          water VARCHAR(128) NOT NULL,
          electric VARCHAR(128) NOT NULL,
          frozen VARCHAR(128) NOT NULL,
          total VARCHAR(128) NOT NULL,
          weight VARCHAR(128) NOT NULL,
          created_date TIMESTAMP
        )`;
  
    pool.query(queryText)
      .then((res) => {
        console.log(res);
        //pool.end();
      })
      .catch((err) => {
        console.log(err);
        //pool.end();
      });
  }